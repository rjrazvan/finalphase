import axios from 'axios'

const SERVER = 'https://webtech-rjrazvan.c9users.io'

class LocationStore{
    constructor(ee){
        this.content = []
        this.ee = ee
        this.selected = null
    }
    getAll(){
        axios(SERVER + '/locations')
        .then((response) => {
            this.content = response.data
            this.ee.emit('LOCATION_LOAD')
        })
        .catch((error) => console.warn(error))
    }
    createOne(location){
        axios.post(SERVER + '/locations', location)
        .then(()=> this.getAll())
        .catch((error) => console.warn(error))
    }
    
    deleteOne(id){
        axios.delete(SERVER + '/location/' + id)
        .then(()=> this.getAll())
        .catch((error) => console.warn(error))
    }
    saveOne(id,location){
        axios.put(SERVER + '/locations/' +id,
        location)
        .then(()=>this.getAll())
        .catch((error) => console.warn(error))
    }
    getOne(id){
        axios(SERVER + '/authors/' + id)
        .then((response) => {
            this.selected = response.data
            this.ee.emit('SINGLE_AUTHOR_LOAD')
        })
        .catch((error) => console.warn(error))
    }
}

export default LocationStore