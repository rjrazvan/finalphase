import React, {Component} from 'react'
import EventStore from './EventStore'
import {EventEmitter} from 'fbemitter'
import Event from './Event'
import EventForm from './EventForm'

let ee = new EventEmitter()
let store = new EventStore(ee)

function addEvent(event){
    store.createOne(event)
}

class EventList extends Component{
    constructor(props){
        super(props)
            this.state= {
                events:[]
        }
    }
    componentDidMount(){
        store.getAll()
        ee.addListener('EVENT_LOAD',()=>{
            this.setState({
                events:store.content
            })
        })
        var MapComponent    = require('react-cartographer/lib/components/Map');
    }
    render(){
        return(
            <div>
            <div>{
            this.state.events.map((e)=><Event event = {e} key={e.id}/>
            )
            }
            </div>
            <EventForm onAdd = {addEvent}/>
            </div>
            )
    }
}
export default EventList