import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import LocationList from './LocationList'
import EventList from './EventList'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Rujoiu Event Manager</h1>
        </header>
        <LocationList/>
        <EventList/>
        <p className="App-intro">
          Welcome to Rujoiu Event Manger, have your event organized like never before.
        </p>
      </div>
    );
  }
}

export default App;
