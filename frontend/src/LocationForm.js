import React, {Component} from 'react'

class LocationForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            locationName : '',
            locationAddres: ''
        }
        this.handleChange = (event) => {
            this.setState({
                [event.target.name] : event.target.value})
            
        }
    }

    render(){
        return(
        <form>
        Location name: <input type = "text" name="locationName" onChange={
            this.handleChange}/>
        Address: <input type = "text" name 
        ="locationAddres" onChange = {this.handleChange}/>
        <input type = "button" value = "add" onClick={()=> this.props.onAdd({name:this.state.locationName,address:this.state.locationAddres})}/>
        </form>
        )
    }
}
 export default LocationForm
    