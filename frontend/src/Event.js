import React, { Component } from 'react'

class Event extends Component {
    constructor(props){
        super(props)
        this.state = {
            isEditing : false
        }
    }
    render(){
        if(!this.state.isEditing){
            return(
                <div>
                {this.props.event.title} description {this.props.event.description} on the date {this.props.event.date  }
                <input type="button" value="edit" onClick={()=>this.setState({isEditing:true})}
        
        />
        </div>
        )
    }
    else{
        return(
            <div>
            <input type="button" value ="cancel" onClick={()=> this.setState({isEditing:false})}/>
            </div>)
    }
    }
}
export default Event