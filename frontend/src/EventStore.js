import axios from 'axios'

const SERVER = 'https://webtech-rjrazvan.c9users.io'

class EventStore{
    constructor(ee){
        this.content = []
        this.ee = ee
        this.selected = null
    }
    
    getAll(){
        axios(SERVER + '/events')
        .then((response) => {
            this.content = response.data
            this.ee.emit('EVENT_LOAD')
        })
        .catch((error)=>console.warn(error))
    }
    createOne(event){
        axios.post(SERVER + '/events', event)
        .then(()=>this.getAll())
        .catch((error)=>console.warn(error))
    }
    
    deleteOne(id){
        axios.delete(SERVER + '/events/' + id)
        .then(()=>this.getAll())
        .catch((error)=>console.warn(error))
    }
    saveOne(id,event){
        axios.put(SERVER+'/events/'+id,event)
        .then(()=>this.getAll())
        .catch((error)=>console.warn(error))
    }
    getOne(id){
        axios(SERVER + '/events/'+id)
        .then((response)=>{
            this.selected = response.data
            this.ee.emit('SINGLE_EVENT_LOAD')
        })
        .catch((error)=>console.warn(error))
    }
}

export default EventStore