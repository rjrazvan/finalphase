import React, {Component} from 'react'

class EventForm extends Component {
    constructor(props){
        super(props)
        this.state={
            eventTitle : '',
            eventDescription : '',
            eventDate: ''
        }
        this.handleChange = (event) => {
            this.setState({
                [event.target.name] : event.target.value
            })
        }
    }
    render(){
        return (
            <form>
            Event title: <input type="text" name = "eventTitle" onChange={this.handleChange}/>
            Description: <input type="text" name ="eventDescription" onchange={this.handleChange}/>
            Date: <input type="date" name="eventDate" onChange={this.handleChange}/>
            <input type="button" value="add"
            onClick={()=> this.props.onAdd({title: this.state.eventTitle,description: this.state.eventDescription,
                date: this.state.eventDate})}/>
            </form>    
            )
    }
}
export default EventForm