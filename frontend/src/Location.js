import React, {Component} from 'react' 
 
class Location extends Component {
    constructor(props){
        super(props)
        this.state = {
            isEditing : false
        }
    }
    render(){
        if(!this.state.isEditing){
            return(
                <div>
                {this.props.location.name} at the location {this.props.location.address}
                <input type = "button" value="edit"
                onClick={()=> this.setState({isEditing:true})}/>
                </div>
                )
        }
        else{
            return(
                <div>
                <input type="button" value="cancel" onClick={()=>this.setState({isEditing:false})}/>
                    </div>
                    )
        }
    }
}

export default Location