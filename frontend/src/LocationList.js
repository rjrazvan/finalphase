import React, {Component} from 'react'
import LocationStore from './LocationStore'
import {EventEmitter} from 'fbemitter'
import Location from './Location'
import LocationForm from './LocationForm'

let ee =  new EventEmitter()
let store = new LocationStore(ee)

function addLocation(location){
    store.createOne(location)
}

class LocationList extends Component{
    constructor(props){
        super(props)
        this.state = {
            locations : []
    }
}

componentDidMount(){
    store.getAll()
    ee.addListener('LOCATION_LOAD',()=>{
        this.setState({
            location : store.content
        })
    })
}
render(){
    return(
        <div>
          <div>
        {
            this.state.locations.map((a)=>
            <Location location = {a} key={a.id}/>)
            }
            </div>
            <LocationForm onAdd={addLocation}/>
            </div>
            )
    }
}

 export default LocationList
