const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require("sequelize")

const sequelize = new Sequelize('rest_messages','root','',{
    dialect : 'mysql',
    define : {
        timestamps : false
    }
})

/*const Organizer = sequelize.define('organizer',{
    name : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [2,20]
        }
    },
    email : {
        type: Sequelize.STRING,
        allowNull: false,
        validate : {
            isEmail : true
        }
    }
},{
    underscored : true
})*/

const Event  = sequelize.define('event',{
    title : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [2,20]
        }
    }, 
    description : {
        type : Sequelize.TEXT,
        allowNull : false,
        validate : {
            len : [10, 1000]
        }
    },
    date : {
        type : Sequelize.DATE,
        allowNull : false,
        defaultValue : Sequelize.NOW
    }
})

const Location = sequelize.define('location',{
    name : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3,30]
      }
    },
    address : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [4,40]
        }
    }
})

Location.hasMany(Event)


const app = express()
app.use(require("cors")())
app.use(bodyParser.json())
app.use(express.static('../frontend/build'))

app.get('/create', (req, res, next) => {
    sequelize.sync({force : true})
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

/*app.get('/organizers',(req,res,next) => {
    Organizer.findAll()
    .then((organizers) => res.status(200).json(organizers))
    .catch((error) => next(error))
})

app.post('/organizers',(req,res,next)=>{
    Organizer.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

*/
app.get('/locations',(req,res,next) => {
    Location.findAll()
    .then((locations) => res.status(200).json(locations))
    .catch((error) => next(error))
})

app.post('/locations', (req,res,next) => {
    Location.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

app.get('/locations/:id',(req,res,next) => {
    Location.findById(req.params.id, {include : [Event]})
    .then((location) => {
        if(location){
            res.status(200).json(location)
        }
        else{
            res.status(404).send('not found')
        }
    })
    .catch((error) => next(error))
})

app.put('/locations/:id', (req,res,next) =>{
    Location.findById(req.params.id)
    .then((location) => {
        if(location){
            return location.update(req.body, {fields:['name','address']})
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then(()=>{
        if(!res.headersSent){
            res.status(201).send('modified')
        }
    })
    .catch((error) => next(error))
})

app.delete('/locations/:id',(req,res,next) =>{
    Location.findById(req.params.id)
    .then((location) => {
        if(location){
            return location.destroy()
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then(() => {
        if(!res.headersSent){
            res.status(201).send('modified')
        }
    })
    .catch((error) => next(error))
})

app.get('locations/:lid/events',(req,res,next) => {
    Location.findById(req.params.aid)
    .then((location) =>{
        if(location){
            return location.getEvents()
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then((events) => {
        if(!res.headersSent){
            res.status(200).json(events)
        }
    })
    .catch((err) => next(err))
})

app.post('/locations/:lid/events',(req,res,next)=>{
    Location.findById(req.params.aid)
    .then((location) => {
        if(location){
            let event = req.body
            event.location_id = location.id
            return Event.create(event)
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then(()=>{
        if(!res.headersSent){
            res.status(201).send('created')
        }
    })
    .catch((err) => next(err))
})

app.get('/locations/:lid/events/:eid',(req,res,next) => {
    Event.findById(req.params.eid)
    .then((event)=>{
        if(event){
            res.status(200).json(event)
        }else{
            res.status(404).sent('not found')
        }
    }).catch((err) => next(err))
})

app.put('/locations/:lid/events/:eid',(req,res,next)=>{
    Event.findById(req.params.id)
    .then((event)=>{
        if(event){
            return event.update(req.body, {fields:['title','description','date']})
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then(() => {
        if(!res.headersSent){
            res.status(201).send('modified')
        }
    })
    .catch((err) => next(err))
})

app.delete('/locations/:lid/events/:eid',(req,res,next)=>{
    Event.findById(req.params.id)
    .then((event)=>{
        if(event){
            return event.destroy()
        }
        else{
            res.status(404).send('not found')
        }
    }).then(()=>{
        if(!res.headersSent){
            res.status((201).sent('removed'))
        }
    })
    .catch((err) => next(err))
})

app.use((err,req,res,next) => {
    console.warn(err)
    res.status(500).send('some error...')
})

app.listen(8080)